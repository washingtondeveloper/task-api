const bodyParser = require('body-parser');

module.exports = app => {
	app.set('PORT', 3000);
	app.use(bodyParser.json());
	app.use((req, res, next) => {
		delete req.body.id;
		next();
	});
};