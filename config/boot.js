module.exports = app => {
	app.db.sync().done(() => {
		app.listen(app.get("PORT"), () => {
			console.log(`Task API - porta ${app.get("PORT")}`);
		});	
	});
};