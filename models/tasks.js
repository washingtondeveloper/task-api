const Sequelize = require('sequelize');

module.exports = app => {
	const sequelize = app.db;
	
	const Tasks = sequelize.define('Tasks', {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		title: {
			type: Sequelize.STRING
		}
	});

	return Tasks;
};