const Sequelize = require('sequelize');

module.exports = app => {
    const sequelize = app.db;
    const Tasks = app.models.tasks;

    const Users = sequelize.define('Users', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
        },
        email: {
            type: Sequelize.STRING,
        }
    });

    Users.hasMany(Tasks);

    return Users;
};