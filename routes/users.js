module.exports = app => {
    const Users = app.models.users;

    app.route('/users')
        .get((req, res) => {
            Users.findAll()
                .then(users => res.json({ users }))
                .catch(error => res.json({ msg: error.message }));
        })
        .post((req, res) => {
            Users.create(req.body)
                .then(user => res.status(201).json({ user }))
                .catch(error => res.json({ msg: error.message }));
        })
    
    app.route('/users/:id')
        .get((req, res) => {

        })
        .put((req, res) => {

        })
        .delete((req, res) => {
            Users.destroy({ where: { id: req.params.id }})
                .then(user => res.status(201).json({ user }))
                .catch(error => res.json({ msg: error.message }));
        });
};