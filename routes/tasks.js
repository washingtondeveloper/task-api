module.exports = app => {

	const Tasks = app.models.tasks;

	app.route('/tasks')
		.get((req, res) => {
			Tasks.findAll()
				.then(tasks => res.json({ tasks }))
				.catch(error => res.json({ msg: error.message }));
		})
		.post((req, res) => {
			Tasks.create(req.body)
				.then(task => res.status(204).json({ task }))
				.catch(error => res.json({ msg: error.message }));
		})

	app.route('/tasks/:id')
		.get((req, res) => {
			Tasks.findById(req.params.id)
				.then(task => {
					if(task)
						res.json({ task });
					else
						res.sendStatus(404);
				})
				.catch(error => res.json({ msg: error.message }));
		})
		.put((req, res) => {
			Tasks.update(req.body, { where:{ id: req.params.id }})
				.then(task => res.sendStatus(204))
				.catch(error => res.json({ msg: error.message }));
		})
		.delete((req, res) => {
			Tasks.destroy({ where: { id: req.params.id }})
				.then(task => res.sendStatus(204))
				.catch(error => res.json({ msg: error.messsage }))
		})

};