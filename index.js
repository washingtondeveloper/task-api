const express = require('express');
const consign = require('consign');

const app = express()

consign()
	.include('db.js')
	.then('models')
	.then('middlewares')
	.then('routes')
	.then('config/boot.js')
	.into(app)